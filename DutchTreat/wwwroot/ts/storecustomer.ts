﻿//export class StoreCustomer {
class StoreCustomer {

    // create private fields firstName/lastName
    constructor(private firstName:string, private lastName:string) {
    }

    // fields
    public visits: number = 1;
    private ourName: string;

    // functions
    public showName() {
        alert(`${this.firstName} ${this.lastName}`);
    }

    // accessors
    set name(val) {
        this.ourName = val;
    }

    get name() {
        return this.ourName;
    }

}

//let cust = new StoreCustomer("Nicolas", "Stohler");
//cust.visits = 10;

//cust.name = "Nicolas";
//cust.showName();

