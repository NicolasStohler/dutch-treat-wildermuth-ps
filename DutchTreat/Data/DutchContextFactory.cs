﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace DutchTreat.Data
{    
    public class DutchContextFactory : IDesignTimeDbContextFactory<DutchContext>
    {
        public DutchContext CreateDbContext(string[] args)
        {
            // Create a configuration 
            //var builder = new WebHostBuilder();
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("config.json")
                .Build();

            var builder = new DbContextOptionsBuilder<DutchContext>();
            var connectionString = config.GetConnectionString("DutchConnectionString");
            builder.UseSqlServer(connectionString);

            //return new DutchContext(new DbContextOptionsBuilder<DutchContext>().Options, config);
            return new DutchContext(builder.Options);
        }
    }
}
