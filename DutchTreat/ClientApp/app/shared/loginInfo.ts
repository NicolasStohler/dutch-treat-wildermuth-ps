﻿export interface LoginInfo {
    token: string;
    expiration: Date;
}
