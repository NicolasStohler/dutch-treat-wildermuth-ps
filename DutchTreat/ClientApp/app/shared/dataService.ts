﻿import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
//import { Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

import { Product } from "./product";
import { Order, OrderItem } from "./order";
import { LoginInfo } from "./loginInfo";

@Injectable()
export class DataService {

    constructor(private http: HttpClient) { }

    private token: string = "";
    private tokenExpiration: Date;

    public products: Product[] = [];
    public order: Order = new Order();

    public loadProducts(): Observable<boolean> {
        return this.http.get<Product[]>("/api/products")
            .map((data: Product[]) => {
                this.products = data;
                return true;
            });
    }

    public get loginRequired(): boolean {
        return this.token.length === 0 || this.tokenExpiration > new Date();
    }

    public login(creds): Observable<boolean> {
        console.log("login...");
        // typesafe
        return this.http.post<LoginInfo>("/account/createtoken", creds)
            .map((li) => { 
                // console.log("post response:", li);
                this.token = li.token;
                this.tokenExpiration = li.expiration;
                return true;
            });

        // without any interface/class:
        //return this.http.post("/account/createtoken", creds)
        //    .map((li: any) => {
        //        // console.log("post response:", li);
        //        this.token = li.token;
        //        this.tokenExpiration = li.expiration;
        //        return true;
        //    });
    }

    public checkout() {
        // without typesafety:
        if (!this.order.orderNumber) {
            this.order.orderNumber = `${this.order.orderDate.getFullYear()}${this.order.orderDate.getTime()}`;
        }
        //console.log("order numer ", this.order.orderNumber);

        return this.http.post("/api/orders", this.order, {
                    headers: new HttpHeaders().set("Authorization", `Bearer ${this.token}`)
                })
            .map((response: any) => {
                this.order = new Order();
                return true;
            });
    }

    public addToOrder(product: Product) {
        let item: OrderItem = this.order.items.find(i => i.productId === product.id);

        if (item) {

            item.quantity++;

        } else {

            item = new OrderItem();
            item.productId = product.id;
            item.productArtist = product.artist;
            item.productCategory = product.category;
            item.productArtId = product.artId;
            item.productTitle = product.title;
            item.productSize = product.size;
            item.unitPrice = product.price;
            item.quantity = 1;

            this.order.items.push(item);
        }
    }
}