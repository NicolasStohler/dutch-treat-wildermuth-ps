﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DutchTreat.Data;
using DutchTreat.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DutchTreat.Controllers
{
    [Route("api/orders/{orderId}/items")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrderItemsController : Controller
    {
        private readonly IDutchRepository _dutchRepository;
        private readonly ILogger<OrderItemsController> _logger;
        private readonly IMapper _mapper;

        public OrderItemsController(IDutchRepository dutchRepository, ILogger<OrderItemsController> logger,
            IMapper mapper)
        {
            _dutchRepository = dutchRepository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get(int orderId)
        {
            try
            {
                var order = _dutchRepository.GetOrderById(User.Identity.Name, orderId);
                if (order != null)
                {
                    return Ok(_mapper.Map<IEnumerable<OrderItemViewModel>>(order.Items));
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error getting items for order: {ex}");
            }
            return BadRequest("Error getting items for order");
        }

        [HttpGet("{id}")]
        public IActionResult Get(int orderId, int id)
        {
            try
            {
                var order = _dutchRepository.GetOrderById(User.Identity.Name, orderId);
                if (order != null)
                {
                    var item = order.Items.FirstOrDefault(i => i.Id == id);
                    if (item != null)
                    {
                        return Ok(_mapper.Map<OrderItemViewModel>(item));
                    }
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error getting items for order: {ex}");
            }
            return BadRequest("Error getting items for order");
        }

        // post...

        // delete...

    }
}
